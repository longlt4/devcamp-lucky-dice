import React from "react";
import Dice from "../assets/images/dice_noun_002_10624.jpg"
import Dice1 from "../assets/images/1.png"
import Dice2 from "../assets/images/2.png"
import Dice3 from "../assets/images/3.png"
import Dice4 from "../assets/images/4.png"
import Dice5 from "../assets/images/5.png"
import Dice6 from "../assets/images/6.png"

class LuckyDice extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            dice: Dice
        }
    }
    onBtnHandlNem = () => {
        console.log("alo")
        let rand = Math.floor(Math.random() * 6) + 1;
        this.setState({
           dice: rand === 1 ? Dice1 : rand === 2 ? Dice2 : rand === 3 ? Dice3 : rand === 4 ? Dice4 : rand === 5 ? Dice5 :  Dice6 })
    }

    render() {
        return (
            <div>
                <img src = {this.state.dice} alt="..."></img>
             
                <button onClick={this.onBtnHandlNem}>Ném</button>
            </div>
        )
    };
}
export default LuckyDice;